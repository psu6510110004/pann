import { useEffect } from 'react';
import { Button } from '@mui/material';
import { Box } from '@mui/system';
import { Login as LoginIcon } from '@mui/icons-material';
import { useAuth } from "react-oidc-context";
import { Navigate, useLocation } from 'react-router-dom';
import { useAppCtx } from '../AppProvider';
import './load.css'

function Login() {
  const Cover = require("../Pic/bg.jpg")
  const { userInfo, action } = useAppCtx();
  const auth = useAuth();
  const location = useLocation();

  console.log('rendering..... login', auth.user)
  useEffect(() => {
    if (auth.isAuthenticated) {
      setTimeout(() => {
        action.setUserInfo({
          ready: true,
          username: auth.user?.profile.preferred_username,
          displayName: auth.user?.profile.given_name + ' ' + auth.user?.profile.family_name
        })
      }, 1000)
    }
  }, [auth, userInfo.ready])

  switch (auth.activeNavigator) {
    case "signinSilent":
      return <div className = 'load' ></div>
    case "signoutRedirect":
      return <div className = 'load' ></div>
  }

  if (auth.isLoading) {
    return <div className = 'load' ></div>
  }

  if (auth.error) {
    return <div className = 'load' ></div>
  }

  if (auth.isAuthenticated) {
    if (userInfo.ready) {
      const backTo = location.state?.backTo || '/home'
      if(action.isStaff()){
        return(
            <Navigate to = '/announcement' replace />
        )    
    }
    return (
      <Navigate to={backTo} replace />
    )
  } else {
    return <div className = 'load' ></div>      
}

}

  return (
    <Box sx={{ 
      display: 'flex', 
      alignItems: 'center', 
      justifyContent: 'center', 
      minHeight: 700, 
      backgroundImage: `url(${Cover})`, 
      backgroundPosition: 'center', 
      backgroundSize: 'cover', 
      backgroundRepeat: 'no-repeat'
    }}>
      <Button variant='contained' sx={{ fontSize: 'large' }} onClick={() => void auth.signinRedirect()}>
        <LoginIcon sx={{ mr: 1 }} />
        Log in
      </Button>
    </Box>
  );
};

export default Login;